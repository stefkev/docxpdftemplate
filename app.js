var express = require("express"),
    bodyParser = require("body-parser"),
    fs = require("fs"),
    lib = require("./lib"),
    validUrl = require('valid-url'),
    request = require("request"),
    archiver = require("archiver");

var mashapeKey = "735047350";
var app = express();
var template = "template.docx";
app.use(bodyParser.urlencoded({ extended: false }))


app.get('/',function(req, res) {
    console.log(req.query.url);
    if(validUrl.isUri(req.query.url)){
        request.get(req.query.url, function(err, resp, body){
            new lib.writeToDoc(body, function(docxData) {
                new lib.generatePdf(docxData, function (pdfData) {

                    var output = fs.createWriteStream(__dirname + '/output/' + docxData.id + '.zip');

                    var archive = archiver('zip');

                    console.log(docxData.path);
                    console.log(pdfData.path)

                    archive
                        .append(fs.createReadStream(docxData.path),{name: docxData.id+".docx"})
                        .append(fs.createReadStream(pdfData.path),{name: pdfData.id+".pdf"})
                        .finalize();

                    archive.pipe(output);

                    output.on('finish', function () {
                        res.download(__dirname + "/output/" + docxData.id + ".zip", function (err) {
                            lib.fileJunkCleaner(__dirname + "/output/" + docxData.id + '.pdf');
                            lib.fileJunkCleaner(__dirname + "/output/" + docxData.id + '.docx')
                            lib.fileJunkCleaner(__dirname + "/output/" + docxData.id + ".zip")
                        })
                    })
                })
            })
        })
    } else{
        res.json({
            error: "invalid url"
        })
    }
});
    app.listen(3000);
