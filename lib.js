var Docgen = require("docxtemplater"),
    ImageModule = require("docxtemplater-image-module"),
    shortId = require("shortid"),
    request = require("request"),
    fs = require("fs"),
    async = require("async");


var nameOfTemplate = "template.docx"
//CHANGE API KEY HERE
var apiKey =  "861855910";
//============================

var imageDownloader = function(path, cb){
    var id = shortId.generate();
    var stream = fs.createWriteStream(__dirname+'/output/'+id+'.jpg');
    stream.uncork();
    request.get(path).pipe(stream).end();
    //stream.once('drain',imageDownloader)
    stream.on('finish',function(){
        cb('output/'+id+'.jpg')
    })
    stream.on('err',function(err){
        console.log(err);
    })
}



module.exports.writeToDoc = function(json, cb){
    var content = fs.readFileSync(__dirname+"/template/"+nameOfTemplate,"binary");
    var id = shortId.generate();

    var imageModule = new ImageModule({centered:false})
    docx = new Docgen().attachModule(imageModule).load(content);

    var data = JSON.parse(json)
    var version = data.version;
    var client = data.client;
    var company = data.company;
    var projectName = data.projectName;
    var projectId = data.projectId;
    var todayDate = new Date();
    var notes = data.notes;
    var details = data.data


    async.eachSeries(details, function(detail, cb){
        var id = shortId.generate();

        //imageDownloader(detail.image, function(img){
        //    detail.localImage = img
        //    cb();
        //})

        console.log('downloading '+id)

        var stream = fs.createWriteStream(__dirname+'/output/'+id+'.jpg');
        request.get(detail.image).pipe(stream);
        detail.localImage = 'output/'+id+'.jpg';
        stream.on('finish',function(){
            cb()
        })

    },function(){
        docx.setData({
            "version": String(version),
            "projectName": String(projectName),
            "projectId": String(projectId),
            "company": String(company),
            "todayDate": String(todayDate),
            "client": String(client),
            "notes": String(notes),
            "data": details
        });
        console.log('generating doc')
        docx.render();
        var buf = docx.getZip().generate({type:"nodebuffer"});

        fs.writeFileSync(__dirname+"/output/"+id+".docx", buf);
        buf.slice(0,0)
        console.log('docx generated')
                details.forEach(function(detail){
                    fileJunkCleaner(__dirname+'/'+detail.localImage)
                })

        cb({
            path: __dirname+'/output/'+id+'.docx',
            id : id
        })

    });

}

module.exports.generatePdf = function(path, cb){
    console.log('Uploading and generating pdf')
    var id = shortId.generate();
    var formData = {
        ApiKey : apiKey,
        File : fs.createReadStream(path.path)
    }

    var streamPdf =  fs.createWriteStream('output/'+path.id+'.pdf')
    request.post({uri: "http://do.convertapi.com/Word2Pdf", formData: formData}).pipe(streamPdf);

    streamPdf.on('finish',function(){
        console.log('pdf generated')
        cb({
            path: __dirname+'/output/'+path.id+'.pdf',
            id: id
        })
    })
}
module.exports.fileJunkCleaner =  fileJunkCleaner = function(path){
    fs.unlink(path, function(err){
        if(err){
            return console.log("error cleaning on error "+err)
        }
        console.log("cleaned on path "+path)
    })
}